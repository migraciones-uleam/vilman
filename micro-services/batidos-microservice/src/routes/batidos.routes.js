const { Router } = require('express');

module.exports = function ({ BatidosController }) {
    const router = Router();
    router.get('/:Id', BatidosController.get);
    router.get('', BatidosController.getAll);
    router.post('/', BatidosController.create);
    router.patch('/:Id', BatidosController.update);
    router.delete('/:Id', BatidosController.delete);

    return router;
}
