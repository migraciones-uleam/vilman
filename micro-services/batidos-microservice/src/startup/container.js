const { createContainer, asClass, asValue, asFunction } = require('awilix');

const config = require('../config');
const app = require('./index');

// importar servicios batidos
const { BatidosService } = require('../services');

// importar controladores
const { BatidosController } = require('../controllers');

// importar rutas
const Routes = require('../routes')
const { BatidosRoutes } = require('../routes/index.routes');

// models
const { BatidosModel} = require('../models');

// repositorios
const { BatidosRepository} = require('../repositories');

const container = createContainer();
container
    .register(
        {
            app: asClass(app),
            router: asFunction(Routes).singleton(),
            config: asValue(config)
        }
    )
    .register(
        {
            BatidosService: asClass(BatidosService).singleton()
        }
    ).register(
        {
            BatidosController: asClass(BatidosController.bind(BatidosController)).singleton()
        }
    ).register(
        {
            BatidosRoutes: asFunction(BatidosRoutes).singleton()

        }
    ).register(
        {
            Batidos: asValue(BatidosModel)
        }
    ).register(
        {
            BatidosRepository: asClass(BatidosRepository).singleton()
        }
    )


module.exports = container;