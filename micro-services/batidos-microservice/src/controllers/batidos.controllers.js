let _batidosService = null;

class BatidosController {
    constructor({ BatidosService }) {
        _batidosService = BatidosService;
    }
    // get
    async get(req, res) {
        const { batidosId } = req.params;
        const batidos = await _batidosService.get(batidosId);
        return res.send(batidos);
    }

    // getAll
    async getAll(req, res) {
        const batidos = await _batidosService.getAll();
        return res.send(batidos);
    }

    // create
    async create(req, res) {
        const { body } = req;
        const createdBatidos = await _batidosService.create(body);
        return res.send(createdBatidos);
    }

    // update
    async update(req, res) {
        const { body } = req;
        const {batidosId } = req.params;
        const updatedBatidos = await _batidosService.update(batidosId, body);
        return res.send(updatedBatidos);
    }
    // delete
    async delete(req, res) {
        const { batidosId } = req.params;
        const deletedBatidos = await _batidosService.delete(batidosId);
        return res.send(deletedBatidos);

    }
}

module.exports = BatidosController;