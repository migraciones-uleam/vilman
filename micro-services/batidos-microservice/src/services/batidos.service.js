const BaseService = require('./base.service')
let _batidosRepository = null;

class BatidosService extends BaseService {
    constructor({ BatidosRepository }) {
        super(BatidosRepository);
        _batidosRepository = BatidosRepository;
    }
    async getBatidosByBatidosName(batidosname) {
        return await _batidosRepository.getBebidasByBebidasName(batidosname);
    }
}

module.exports = BatidosService;