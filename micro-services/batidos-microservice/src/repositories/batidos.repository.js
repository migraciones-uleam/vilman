const BaseRepository = require('./base.repository');
let _batidos= null;

class BatidosRepository extends BaseRepository {
    constructor({Batidos}){
        super(Batidos);
        _batidos = Batidos
    }
    async getBatidosByBatidosName(batidosname){
        return await _batidos.findOne({batidosname});
    }
}

module.exports = BatidosRepository;