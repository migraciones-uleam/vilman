var express = require('express');
var router = express.Router();

const { BatidosController } = require('../controllers/index');

router.get('/', BatidosController.getBatidos);
router.post('', BatidosController.postBatidos);

module.exports = router;
