var express = require('express');
var router = express.Router();

const { Salchipapacontroller } = require('../controllers/index');

router.get('/', Salchipapacontroller.getSalchipapa);
router.post('', Salchipapacontroller.postSalchipapa);

module.exports = router;
