var express = require('express');
var router = express.Router();

const { BebidasController } = require('../controllers/index');

router.get('/', BebidasController.getBebidasn);
router.post('', BebidasController.postBebidas);

module.exports = router;
