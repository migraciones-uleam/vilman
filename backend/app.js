var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var morgan = require('morgan');
var methodOverride = require('method-override');

var indexRouter = require('./routes/index');
var bebidasRouter = require('./routes/bebidas');
var salchipapaRouter = require('./routes/salchipapa');
var hamburRouter = require('./routes/hambur');
var BatidosRouter = require('./routes/batidos');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(morgan('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use(methodOverride('_method'));

app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/bebidas', bebidasRouter);
app.use('/salchipapa', salchipapaRouter);
app.use('/add', salchipapaRouter);
app.use('/anadir', bebidasRouter);
app.use('/hambur', hamburRouter);
app.use('/nuevo', hamburRouter);
app.use('/batidos', BatidosRouter);
app.use('/adicional', BatidosRouter);
module.exports = app;
