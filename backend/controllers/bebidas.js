var axios = require("axios").default;
const url = "http://api:5000/v1/api/bebidas";



async function getBebidasn(req, res, next) {
    const bebidas = await axios.get(url); 
    res.render('bebidas',  { title: 'Bebidas', Bebidas: bebidas.data });
}

async function postBebidas(req, res, next) {
    const { orden, nombre, precio, cantidad } = req.body;
    await axios.post(url, { orden, nombre, precio, cantidad });
    res.redirect('bebidas');
}


module.exports = {
    getBebidasn,
    postBebidas
}