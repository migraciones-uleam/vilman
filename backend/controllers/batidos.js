var axios = require("axios").default;
const url = "http://batidos:3131/v1/api/batidos";



async function getBatidos(req, res, next) {
    const batidos = await axios.get(url); 
    res.render('batidos',  { title: 'Batidos', Batidos: batidos.data });
}

async function postBatidos(req, res, next) {
    const { orden, nombre, precio, descripcion } = req.body;
    await axios.post(url, { orden, nombre, precio, descripcion });
    res.redirect('batidos');
}


module.exports = {
    getBatidos,
    postBatidos
}