const { Router } = require('express');

module.exports = function ({ BebidasController }) {
    const router = Router();
    router.get('/:bebidasId', BebidasController.get);
    router.get('', BebidasController.getAll);
    router.post('/', BebidasController.create);
    router.patch('/:bebidasId', BebidasController.update);
    router.delete('/:bebidasId', BebidasController.delete);

    return router;
}
