const { createContainer, asClass, asValue, asFunction } = require('awilix');

const config = require('../config');
const app = require('./index');

// importar servicios HamburRouter
const { SalchipapaService, BebidasService, HamburService } = require('../services');

// importar controladores
const { SalchipapaController, BebidasController, HamburController } = require('../controllers');

// importar rutas
const Routes = require('../routes')
const { SalchipapaRoutes, BebidasRoutes, HamburRoutes } = require('../routes/index.routes');

// models
const { SalchipapaModel, BebidasModel, HamburModel} = require('../models');

// repositorios
const { SalchipapaRepository, BebidasRepository, HamburRepository} = require('../repositories');



const container = createContainer();
container
    .register(
        {
            app: asClass(app),
            router: asFunction(Routes).singleton(),
            config: asValue(config)
        }
    )
    .register(
        {
            SalchipapaService: asClass(SalchipapaService).singleton(),
            BebidasService: asClass(BebidasService).singleton(),
            HamburService: asClass(HamburService).singleton()
          
        }
    ).register(
        {
            SalchipapaController: asClass(SalchipapaController.bind(SalchipapaController)).singleton(),
            BebidasController: asClass(BebidasController.bind(BebidasController)).singleton(),
            HamburController: asClass(HamburController.bind(HamburController)).singleton()

        }
    ).register(
        {
            SalchipapaRoutes: asFunction(SalchipapaRoutes).singleton(),
            BebidasRoutes: asFunction(BebidasRoutes).singleton(),
            HamburRoutes: asFunction(HamburRoutes).singleton()

        }
    ).register(
        {
            Salchipapa: asValue(SalchipapaModel),
            Bebidas: asValue(BebidasModel),
            Hambur: asValue(HamburModel)
        }
    ).register(
        {
            SalchipapaRepository: asClass(SalchipapaRepository).singleton(),
            BebidasRepository: asClass(BebidasRepository).singleton(),
            HamburRepository: asClass(HamburRepository).singleton()

        }
    )


module.exports = container;