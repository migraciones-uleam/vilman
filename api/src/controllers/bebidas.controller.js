let _bebidasService = null;

class BebidasController {
    constructor ({ BebidasService}){
        _bebidasService = BebidasService;

    }

    async get (req, res){
        const { bebidasId} = req.params;
        const bebidas = await _bebidasService.get(bebidasId);
        return res.send(bebidas);
    }
    async getAll(req, res) {
        const bebidasn = await _bebidasService.getAll();
        return res.send(bebidasn);
    }
    async create(req, res) {
        const { body } = req;
        const createdBebidas = await _bebidasService.create(body);
        return res.send(createdBebidas);
    }
    // METODO DELETE
    async delete(req, res) {
        const { bebidasId } = req.params;
        const deletedBebidas = await _bebidasService.delete(bebidasId);
        return res.send(deletedBebidas);

}
    //  METODO UPDATE
    async update(req, res) {
        const { body } = req;
        const { bebidasId } = req.params;
        const updatedBebidas = await _bebidasService.update(bebidasId, body);
        return res.send(updatedBebidas);
    }

  
}

module.exports = BebidasController;


