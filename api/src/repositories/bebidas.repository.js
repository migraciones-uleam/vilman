const BaseRepository = require('./base.repository');
let _bebidas= null;

class BebidasRepository extends BaseRepository {
    constructor({Bebidas}){
        super(Bebidas);
        _bebidas = Bebidas
    }
    async getBebidasByBebidasName(bebidasname){
        return await _bebidas.findOne({bebidasname});
    }
}

module.exports = BebidasRepository;