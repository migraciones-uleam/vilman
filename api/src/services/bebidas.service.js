const BaseService = require('./base.service')
let _bebidasRepository = null;

class BebidasService extends BaseService {
    constructor({ BebidasRepository }) {
        super(BebidasRepository);
        _bebidasRepository = BebidasRepository;
    }
    async getBebidasByBebidasName(bebidasname) {
        return await _bebidasRepository.getBebidasByBebidasName(bebidasname);
    }
}

module.exports = BebidasService;